# P1: Multithreading en C++

Elena Romón López

Fecha: 10/12/2019

(Se sube al gitlab el 11/12/19 a las 23:40 aprox.)

## Prefacio

El informe siguiente resume el trabajo realizado durante la implementación de esta primera práctica, así como los objetivos conseguidos. Se comentan también tanto las pruebas realizadas como los resultados obtenidos. Además, se hace una breve mención a las dificultades que se han ido encontrando a lo largo del desarrollo de la práctica.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

En esta ocasión, el benchmarking se ha realizado en mi ordenador personal, con las características que a continuación se mencionan:

- Host:
  - CPU Intel(R) Core(TM) i7-7500U
  - CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket
  - CPU flags:  FALTAAAA
  - CPU frequency: 1.60 GHz (se ha fijado con cpufreq)
  - CPU: L2: 256K, L3: 4096K
  - Trabajo y benchmarks sobre SSD
  - Sin entorno gráfico durante ejecuciones

## 2. Diseño e Implementación del Software

## 2.1. Parte BASE

En primer lugar, para la parte base se ha desarrollado un código que opera un vector de doubles. Este programa recibe de la CLI la operación que se desea realizar (SUM,SUM,XOR) y el tamaño del vector. Además, el programa se podrá ejecutar tanto de manera secuencial como de manera paralela, pudiendo en este segundo caso seleccionar el número de threads con los que se quiere ejecutar. Esto último también se pasará como parámetro por la CLI. Es decir, a la hora de ejecutar el programa se deber ingresar en la CLI lo siguiente:

  - En caso de querer una ejecución secuencial: ./p1 10 sum
  - En caso de querer una ejecución paralela: ./p1 10 sum --multi-thread 4

donde 10 es el número de elementos del array, sum es la operación a realizar, y 4 es el número de threads que se quieren emplear.

El código, en primer lugar, comprueba el número de parámetros que se han introducido en la CLI. Si únicamente se han introducido dos parámetros, se entiende que son el tamaño del array y la operación, y se ejecutará de manera secuencial, siempre y cuando la operación sea una de las tres definidas. Por otro lado, si se han introducido cuatro parámetros, se comprueba que dos son los ya mencionados, y que los otros son "--multi-thread" para indicar que el siguiente parámetro es el número de threads a ejecutar, y el número de threads (que se debe encontrar entre 1 y 12), y se ejecutará de forma paralela.

La ejecución del código de forma secuencial no presenta mayor dificultad: se hace una llamada a la función correspondiente a la operación que se quiere realizar, y esta recorre el bucle operando los elementos y guarda el resultado final en una variable global.

Sin embargo, para ejecutar el programa de forma paralela hay que crear tantos threads como se haya indicado en la CLI, y el trabajo se ha de repartir de forma equitativa. Para ello, se ha implementado una función que calcula cuántos elementos debe operar cada thread. En caso de que la división del trabajo no sea exacta, los elementos restantes los calculará el thread que primero finalice su ejecución. Para ello, se han creado los threads como se muestra en el siguiente bloque de código:

```c++
std::thread threads[numThreads];

if(strcmp(argv[2], "sum") == 0) {
  operacion = 1;
  for(auto i=0; i<numThreads; ++i){
    threads[i] = std::thread(operacionParalela,operacion,numEleT,restoEle,numThreads,array,i);
  }
} else if (strcmp(argv[2], "sub") == 0) {
...
} else if (strcmp(argv[2], "xor") == 0){
...
} else {
  printf("ERROR: La operación introducida no es válida.\n");
  exit(0);
}

```

Una vez creados los threads, estos harán una llamada a la función asociada a la ejecución de los threads, a la que se le pasará el rango de valores del array con el que se debe operar. Esta función se encarga a su vez llamar a la función de la operación requerida, así como de comprobar si el thread que se está ejecutando es el primero en finalizar, y en ese caso, computa también los elementos restantes. Esto lo vemos en el siguiente fragmento de código:

```c++

if (op == 1){
    r = funSuma(th*numEleT,numEleT,array);
    if (esElPrimero == false){
      esElPrimero = true;
      inicio = numThreads*numEleT;
      r += funSuma(inicio,resto,array);
    }
...
}

```

Además, a la hora de guardar el resultado que retornan las funciones de operación en la variable global, hay que tener cuidado de que dos threads no cambien su valor a la vez. Para ello, se emplea un mutex tal y como vemos en el siguiente fragmento de código.

```c++
std::lock_guard<std::mutex> guard(m);
resul += r;
```

Por último, para poder comparar los tiempos de ejecución de las zonas críticas se ha utilizado la función gettimeofday, que retorna la hora actual. Esto se ha hecho tanto al comienzo de las zonas críticas como al final, pudiendo así al final de la ejecución restar ambos valores, obteniendo el tiempo transcurrido durante la ejecución. un ejemplo de su uso sería el siguiente:

```c++
gettimeofday( &t, NULL );
tiempo = (double)t.tv_sec + (double)t.tv_usec / 1000000.0;
// Bloque de código crítico
```

## 2.2. Logger

En este apartado se pide un thread que se encargue de recoger los resultados del resto de threads a medida que estos van acabando, y que se encargue de sumarlos. Esto se tiene que implementar utilizando compilación condicional, para que cuando no se quiera realizar esta función no perjudique al rendimiento del programa. Esto se consigue colocando todos los fragmentos de código necesarios para esta parte en bloques como el siguiente, que crea los atributos necesarios para ejecutar el código perteneciente al logger.

```c++
#if FEATURE_LOGGER
      std::thread logger;
      double resultadosLogger[numThreads];
      puntero_logger = resultadosLogger;
      std::condition_variable condVar_Logger;
      bool finThreads = false;
      logger = std::thread(funcionLogger,operacion,numThreads,numEleArray,array,&condVar_Logger,&resulLogger,&finThreads);
#endif
```

Por otro lado, en esta parte se han de guardar los resultados parciales de cada thread en un array, de manera que cuando todos los threads hayan acabado su ejecución, el logger lo leerá y calculará el resultado final. En el caso del thread que ejecuta el resto resultante (en caso de haberlo), en el array guardará calculada ya la suma de su parte más lo del resto.

El thread del logger ejecutará una función exclusiva para él, que se encarga de calcular el resultado final de la ejecución paralela, así como de calcular la forma secuencial, para luego compararlo en el main. A continuación se muestra parte del código de la función que ejecuta el logger.

```c++
#if FEATURE_LOGGER
void funcionLogger(int operacion,int numThreads, int numEleArray, double *array, std::condition_variable *condVar_Logger, double *resulLogger, bool *finThreads){

  // Espera que todos los threads terminen
  while(numFinalizados != numThreads){
    std::unique_lock<std::mutex> m_logger(m);
    condVar.wait(m_logger, []{return g_ready;});
    g_ready = false;
    m_logger.unlock();
  }
    ...

  double r_secuencial = 0;
  if (operacion == 1){
    r_secuencial = funSuma(0,numEleArray,array);
  } else if (operacion == 2){
    r_secuencial = funResta(0,numEleArray,array);
  } else if (operacion = 3){
    r_secuencial = (int)funXor(0,numEleArray,array);
  } else {
    printf("ERROR: La operación introducida no es válida.\n");
    exit(0);
  }
  printf("El resultado calculado de forma secuencial es %f.\n", r_secuencial);
  std::lock_guard<std::mutex> guard(m);
  *resulLogger = r_secuencial;
  *finThreads = true;
  condVar_Logger->notify_one();
}
#endif
```
Nota: en la explicación se habla de la suma de los resultados de los threads porque en las pruebas del programa generalmente se ha trabajado con esa operación, y por simplificar.

## 2.3. Optimización del paralelismo

Para optimizar la parte paralela se ha utilizado la compilación condicionada FEATURE_OPTIMIZE. Cuando se ejecutan estas áreas, el resultado se guarda como una variable atómica, siendo mucho más fácil de operar computacionalmente hablando. Por tanto, con esto se consigue una mayor rapidez al operar los resultados del thread, no haciendo falta por tanto el mutex. Esto hace que el código sea más óptimo. Para ello, se han implementado fragmentos de código como el siguiente:

```c++
#if FEATURE_OPTIMIZE
  #include <atomic>
  std::atomic<double> resul(0);
#else
  double resul = 0;
#endif
...
if (op == 1){
    r = funSuma(th*numEleT,numEleT,array);
      ...
    #if FEATURE_OPTIMIZE
        resul=resul+r;
    #else
        std::lock_guard<std::mutex> guard(m);
        resul += r;
    #endif
}
```

## 2.4. Conector con java

Esta parte de la práctica no se ha conseguido implementar, y por tanto no se comentará nada de ella en este informe.

## 3. Metodología y desarrollo de las pruebas realizadas

En primer lugar, para comprobar el correcto funcionamiento de la parte base se ha trabajado con un array pequeño, de 10 elementos, para poder así comprobar de forma manual que el resultado obtenido era el deseado, tanto en el caso de la ejecución secuencial como en el de la ejecución paralela.

A partir de ahí, se han realizado pruebas con dos longitudes de arrays (10024 y 10000024), ejecutando tanto de manera secuencial, como de manera paralela con 1,2 y 4 threads. Esto se ha hecho en dos estaciones, y entre ejecuciones se ha esperado 1 segundo. Además, la primera ejecución no se ha considerado a la hora de comparar los resultados (se deja como calentamiento de la máquina).

De todo esto, se han tomado los tiempos de ejecución de las zonas críticas con la ayuda del benchmark que figura más adelante, y se han realizado algunas gráficas comparativas.

En primer lugar comparamos los tiempos de ejecución en dos estaciones de una suma para un array de 10024. Se han representado los tiempos tanto para la ejecución secuencial como el secuencial para 1, 2 y 4 threads. Vemos que para este número relativamente pequeño los tiempos no mejoran tras la ejecución secuencial, aunque sí que mejoran algo en la segunda estación respecto a la primera.

<img src="paraleloPequeño.png" width="500" style="display:block; margin:0 auto;"/>

Por otro lado, realizamos la misma comparativa para un numero mayor de elementos (10000024 en este caso), y en este caso sí que se nota una mejoría con la ejecución paralela. Esto no ocurre en la ejecución paralela con un único thread por el trabajo que conlleva tanto de la creación de hilos, como de mutex, etc.

<img src="paraleloGrande.png" width="500" style="display:block; margin:0 auto;"/>

A continuación, se ha realizado una comparativa de la ejecución de la suma para un array de 10024 elementos, por un lado de manera paralela frente a la ejecución paralela con el FEATURE_OPTIMIZE activado, tanto para 1,2 como 4 threads. Vemos que para un número razonablemente pequeño se la mejoría con el uso del optimize es notable.

<img src="optimizePequeño.png" width="500" style="display:block; margin:0 auto;"/>

Se ha realizado la misma comparativa también para un array de mayor tamaño (10000024 elementos). En este caso, es muy destacable que la ejecución con un thread es altísima en el paralelo, mientras que el optimize, aun siendo la que tiene un tiempo mayor, el tiempo es mucho menor. Deducimos por tanto que en su mayoría, lo que le pesa a la ejecución de un thread del paralelo es principalmente el trabajo que el optimize retira de la ejecución.

<img src="optimizeGrande.png" width="500" style="display:block; margin:0 auto;"/>

Hubiera sido una mejor opción realizar estas comparaciones con el speedup, así como con más estaciones, y no se ha hecho por falta de tiempo. Además, tal y como expuse en la pequeña presentación del informe hace unas semanas, me parecía interesante realizar la comparativa de los resultados de las diferentes operaciones. Tampoco se ha realizado por falta de tiempo. Así mismo, me hubiera gustado hacer comparativas con el uso de la función logger, pero no lo veía tan esencial para un análisis rápido de los resultados.

A continuación se muestra el benchmark utilizado:

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
size1=100000
size2=10000000
case "$1" in
  seq1)
      ./p1 $size1 sum
      ;;
  seq2)
      ./p1 $size2 sum
      ;;
  mul1)
    ./p1 $size1 sum --multi-thread "$2"
    ;;
  mul2)
    ./p1 $size2 sum --multi-thread "$2"
    ;;
esac
```




**launcher.sh:**

```sh
file=resultsInc.csv
touch $file
	for size in seq1 seq2; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done
	for size in mul1 mul2; do
	    for nThreads in 1 2 4 8; do
		printf "$size NT:$nThreads," >> $file
		for i in `seq 1 1 11`;
		do
		    ./benchsuite.sh $size $nThreads >> $file # results dumped
		done
		sleep 1
		printf "\n" >> $file
	    done
	done
```

## 4. Discusión

## 4.1. Valoración personal de la práctica

El desarrollo de esta práctica me ha parecido bastante duro teniendo en cuenta que sesiones de laboratorio como tal para dedicarle a la práctica se ha dedicado sólo una. En mi opinión hubiera sido más conveniente quizás dedicarle un par de sesiones a esta, ya que es la que más elaboración de código lleva, hay que tener en cuenta muchas cosas, y en las dos horas de la sesión apenas dio tiempo, en mi caso, a hacer la parte secuencial.

Comprendo que los alumnos que la han estado trabajando todos los días han tenido tiempo de sobra para preguntar dudas en clase y/o tutorías, pero la gente que va más apurada y tiene que trabajar además en las prácticas de otras asignaturas, hubieran preferido dedicar alguna sesión más de clase al desarrollo de esta práctica.
