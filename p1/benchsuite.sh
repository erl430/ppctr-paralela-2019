#!/usr/bin/env bash
size1=100000
size2=10000000
case "$1" in
  seq1)
      ./p1 $size1 sum
      ;;
  seq2)
      ./p1 $size2 sum
      ;;
  mul1)
    ./p1 $size1 sum --multi-thread "$2"
    ;;
  mul2)
    ./p1 $size2 sum --multi-thread "$2"
    ;;
esac
