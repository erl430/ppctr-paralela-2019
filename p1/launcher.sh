file=resultsInc.csv
touch $file
	for size in seq1 seq2; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done
	for size in mul1 mul2; do
	    for nThreads in 1 2 4 8; do
		printf "$size NT:$nThreads," >> $file
		for i in `seq 1 1 11`;
		do
		    ./benchsuite.sh $size $nThreads >> $file # results dumped
		done
		sleep 1
		printf "\n" >> $file
	    done
	done
