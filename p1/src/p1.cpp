#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <sys/time.h>

#define FEATURE_LOGGER 0
#define FEATURE_OPTIMIZE 0


/*
 * Práctica 1. Multithreading en c++
 * @author: Elena Romón López
 * @version dic-2019.
 */

// Declaracion de funciones
double funSuma(int ini, int numEleT, double *array);
double funResta(int ini, int numEleT, double *array);
double funXor(int ini, int numEleT, double *array);

// Parametros: indice de la operacion, número de elementos para cada thread, resto para el primer thread, número de threads, numero del thread
double operacionParalela(int op, int numEleT, int resto, int numThreads, double *array, int th);

// Calcula el número de elementos que trabajará cada thread
int numEleThread(int numEleArray, int numThreads);

// Función logger
void funcionLogger(int operacion, int numThreads, int numEleArray, double *array, std::condition_variable *condVar_Logger, double *resulLogger, bool *finThreads);

#if FEATURE_OPTIMIZE
  #include <atomic>
  std::atomic<double> resul(0);
#else
  double resul = 0;
#endif

double resultado_final;
bool esElPrimero = false;
std::mutex m;
double tiempo;
struct timeval t;

// Logger
std::condition_variable condVar;
bool g_ready = false;
double *puntero_logger;
int numFinalizados;
double resulLogger;


/**** Función principal ****/
int main(int argc, char *argv[]){
  // Comprobación de los parámetros introducidos
  if (argc != 3 && argc != 5){
    printf("Los parámetros introducidos están mal.\n");
    printf("%d\n",argc );
    exit(0);
  }

  int numEleArray = atoi(argv[1]);

  printf("numEleArray= %d, operacion %s\n",numEleArray, argv[2]);

  /*  Ejecución paralela */
  if (argc == 5){
  if (strcmp(argv[3],"--multi-thread") == 0){
    resul= 0;
    //printf("Estoy en el paralelo\n");

    int numThreads = atoi(argv[4]);

    // Comprobación del número de threads
    if (numThreads < 1 || numThreads > 12){
      printf("ERROR: número de threads incorrecto.\n");
      exit(0);
    }

    double *array = (double*) malloc(sizeof(double)*numEleArray);
    // Inicializar el array
    for (int i = 0; i <numEleArray; i++){
      array[i] = i;
    }

    int operacion;
    // Cálculo del número de elementos para cada thread
    int numEleT = numEleThread(numEleArray, numThreads);

    // Cálculo del resto
    int restoEle = numEleArray - (numEleT*numThreads);

    gettimeofday( &t, NULL );
    tiempo = (double)t.tv_sec + (double)t.tv_usec / 1000000.0;

    std::thread threads[numThreads];

    if(strcmp(argv[2], "sum") == 0) {
      operacion = 1;
      for(auto i=0; i<numThreads; ++i){
        threads[i] = std::thread(operacionParalela,operacion,numEleT,restoEle,numThreads,array,i);
      }
    } else if (strcmp(argv[2], "sub") == 0) {
      operacion = 2;
      for(auto i=0; i<numThreads; ++i){
        threads[i] = std::thread(operacionParalela,operacion,numEleT,restoEle,numThreads,array,i);
      }
    } else if (strcmp(argv[2], "xor") == 0){
      operacion = 3;
      for(auto i=0; i<numThreads; ++i){
        threads[i] = std::thread(operacionParalela,operacion,numEleT,restoEle,numThreads,array,i);
      }
    } else {
      printf("ERROR: La operación introducida no es válida.\n");
      exit(0);
    }

#if FEATURE_LOGGER
      std::thread logger;
      double resultadosLogger[numThreads];
      puntero_logger = resultadosLogger;
      std::condition_variable condVar_Logger;
      bool finThreads = false;
      logger = std::thread(funcionLogger,operacion,numThreads,numEleArray,array,&condVar_Logger,&resulLogger,&finThreads);
#endif

    for(int i=0; i<numThreads;++i){
      threads[i].join();
    }

#if FEATURE_LOGGER
  std::unique_lock<std::mutex> guard(m);
  condVar_Logger.wait(guard, [&finThreads]{return finThreads;});
  guard.unlock();
  logger.join();
#endif
  } else {
    printf("ERROR: Los parámetros introducidos no son válidos.\n");
    exit(0);
  }
} else {
    resul = 0;
    double *array = (double*) malloc(sizeof(double)*numEleArray);
    // Inicializar el array
    for (int i = 0; i <numEleArray; i++){
      array[i] = i;
    }

    gettimeofday( &t, NULL );
    tiempo = (double)t.tv_sec + (double)t.tv_usec / 1000000.0;

    if(strcmp(argv[2], "sum") == 0){
      resul = funSuma(0,numEleArray,array);
    } else if (strcmp(argv[2], "sub") == 0) {
      resul = funResta(0,numEleArray,array);
    } else if (strcmp(argv[2], "xor") == 0){
      resul = funXor(0,numEleArray,array);
    } else {
      printf("ERROR: La operación introducida no es válida.\n");
      exit(0);
    }
  }
#if FEATURE_LOGGER
  if(resul == resulLogger){
    printf("Los resultados del logger y del main coinciden.\n");
  } else {
    printf("ERROR: Los resultados del logger y del main no coinciden.\n");
    exit(0);
  }
#endif

  gettimeofday( &t, NULL );
  double tFinal = (double)t.tv_sec + (double)t.tv_usec / 1000000.0;

  #if FEATURE_OPTIMIZE
    std::cout << "Resultado = "<< resul << "Tiempo transcurrido = " << tFinal-tiempo <<"\n";
  #else
    std::cout << "Resultado = " << resul << "Tiempo transcurrido = " << tFinal-tiempo <<"\n";
  #endif
}

// Calculo del numero de elementos que va trabajar cada array
int numEleThread(int numEleArray, int numThreads){
  return numEleArray/numThreads;
}

double operacionParalela(int op, int numEleT, int resto, int numThreads, double *array, int th){
  double r;
  int inicio;

  if (op == 1){
      r = funSuma(th*numEleT,numEleT,array);
      if (esElPrimero == false){
        esElPrimero = true;
        inicio = numThreads*numEleT;
        r += funSuma(inicio,resto,array);
      }
      #if FEATURE_OPTIMIZE
          resul=resul+r;
      #else
          std::lock_guard<std::mutex> guard(m);
          resul += r;
      #endif
  } else if (op == 2) {
      r = funResta(th*numEleT,numEleT,array);
      if (esElPrimero == false){
        esElPrimero = true;
        inicio = numThreads*numEleT;
        r += funResta(inicio,resto,array);
      }
      #if FEATURE_OPTIMIZE
          resul=resul+r;
      #else
          std::lock_guard<std::mutex> guard(m);
          resul += r;
      #endif
  } else if (op == 3) {
      r = funXor(th*numEleT,numEleT,array);
      if (esElPrimero == false){
        esElPrimero = true;
        inicio = numThreads*numEleT;
        r = (int)r ^(int)funXor(inicio,resto,array);
      }
      #if FEATURE_OPTIMIZE
          resul = (int)resul^(int)r;
      #else
          std::lock_guard<std::mutex> guard(m);
          resul = (int)resul^(int)r;
      #endif
  } else {
    printf("ERROR al realizar la operación.\n");
  }
#if FEATURE_LOGGER
    puntero_logger[th] = r;
    numFinalizados++;
    g_ready = true;
    condVar.notify_one();
#endif
  return resultado_final;
}

#if FEATURE_LOGGER
void funcionLogger(int operacion,int numThreads, int numEleArray, double *array, std::condition_variable *condVar_Logger, double *resulLogger, bool *finThreads){

  // Espera que todos los threads terminen
  while(numFinalizados != numThreads){
    std::unique_lock<std::mutex> m_logger(m);
    condVar.wait(m_logger, []{return g_ready;});
    g_ready = false;
    m_logger.unlock();
  }

  // Imprime por pantalla los resultados del paralelo
  for (auto i = 0; i<numThreads; i++){
    printf("Thread = %d, Resultado = %f.\n", i, puntero_logger[i]);
  }

  // Calcula el secuencial
  double r_secuencial = 0;
  if (operacion == 1){
    r_secuencial = funSuma(0,numEleArray,array);
  } else if (operacion == 2){
    r_secuencial = funResta(0,numEleArray,array);
  } else if (operacion = 3){
    r_secuencial = (int)funXor(0,numEleArray,array);
  } else {
    printf("ERROR: La operación introducida no es válida.\n");
    exit(0);
  }
  printf("El resultado calculado de forma secuencial es %f.\n", r_secuencial);
  std::lock_guard<std::mutex> guard(m);
  *resulLogger = r_secuencial;
  *finThreads = true;
  condVar_Logger->notify_one();
}
#endif

/**
* Función que suma los elementos del array entre las ini y fin
**/
double funSuma(int ini, int numEleT, double *array){
  double resultado = 0;
  for(auto i = ini; i<numEleT+ini; i++){
    resultado += array[i];
  }
  return resultado;

}

/**
* Función que resta los elementos del array entre las ini y fin
**/
double funResta(int ini, int numEleT, double *array){
  double resultado = 0;
  for(auto i = ini; i<numEleT+ini; i++){
    resultado = resultado - array[i];
  }
  return resultado;
}

/**
* Función que hace la operación xor los elementos del array entre las ini y fin
**/
double funXor(int ini, int numEleT, double *array){
  double resultado = 0;
  for(auto i = ini; i < numEleT+ini; i++){
    resultado = (int)resultado ^ (int)array[i];      // operacion xor
  }
  return resultado;
}
