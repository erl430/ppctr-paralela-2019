#!/usr/bin/env bash
size1=1000000
size2=100000000
case "$1" in
  seq1)
      build/seq $size1
      ;;
  seq2)
      build/seq $size2
      ;;

  mul11)
      build/cpp $size1 1
      ;;
  mul12)
      build/cpp $size1 2
      ;;
  mul14)
      build/cpp $size1 4
      ;;
  mul18)
      build/cpp $size1 8
      ;;
  mul21)
      build/cpp $size2 1
      ;;
  mul22)
      build/cpp $size2 2
      ;;
  mul24)
      build/cpp $size2 4
      ;;
  mul28)
      build/cpp $size2 8
      ;;

  omp11)
      build/omp $size1 1
      ;;
  omp12)
      build/omp  $size1 2
      ;;
  omp14)
      build/omp  $size1 4
      ;;
  omp18)
      build/omp  $size1 8
      ;;
  omp21)
      build/omp  $size2 1
      ;;
  omp22)
      build/omp  $size2 2
      ;;
  omp24)
      build/omp  $size2 4
      ;;
  omp28)
      build/omp  $size2 8
      ;;

esac
