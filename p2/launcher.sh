file=resultsInc.csv
touch $file
	for size in seq1 seq2; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

	for size in mul11 mul12 mul14 mul18 mul21 mul22 mul24 mul28; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

	for size in omp11 omp12 omp14 omp18 omp21 omp22 omp24 omp28; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

