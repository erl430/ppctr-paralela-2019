# Práctica 2: Optimizing

Autor: Elena Romón López

Fecha: 17/12/2019

## Prefacio

En esta práctica se ha conseguido optimizar el código secuencial proporcionado, tanto utilizando c++ como OpenMP, tal y como se explicará en esta memoria, tras analizar los resultados obtenidos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

En esta ocasión, el benchmarking se ha realizado en mi ordenador personal, con las características que a continuación se mencionan:

- Ejecución sobre sistema operativo Ubuntu 18.10 64 bits.
- Host:
  - CPU Intel(R) Core(TM) i7-7500U
  - CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket
  - CPU flags:  fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d

  - CPU frequency: 1.60 GHz (se ha fijado con cpufreq)
  - CPU: L2: 256K, L3: 4096K
  - Trabajo y benchmarks sobre SSD
  - Sin entorno gráfico durante ejecuciones

  Nota: en el informe del apartado 1 no figuran las flags de la CPU, pero son las mismas que las indicadas en este.

## 2. Diseño e Implementación del Software

## 2.1. BASE

En primer lugar, en el caso base se comprueban los parámetros introducidos. En este caso, solo se pueden recibir 2 parámetros, o 3 en caso de querer ejecución paralela (para esta parte BASE no se utilizará). Si se recibe un número diferente de parámetros, salta un mensaje de error y se finaliza la ejecución. Además, se comprueba que el número de iteraciones sea mayor que 0.

Para mejorar la eficiencia del programa proporcionado, se ha eliminado el uso del struct para el paso de los datos, así como las funciones "attach", "parse" y "sequential", que en el código entregado no son necesarias. Por tanto, atributos como gap y resul ahora deben ser inicializados al comienzo del main. En esta implementación también se ha eliminado el fichero "p2.hpp", ya que al eliminar las estructuras y las funciones ya mencionadas, no resultaba necesario.

Además, las operaciones que se realizaban de manera muy enrevesada con los valores del struct, y algunas otras variables constantes para todo el código, se han simplificado, quedando finalmente en una únicamente en un par de líneas que se utilizarán en todas las versiones del código, que se verán un poco más adelante.

Todo lo comentado hasta ahora va a ser común a cualquiera de los modos que se ejecuten. Ahora, como estamos en el caso base, se habrá compilado de forma condicional con MODE = 0. Con esto se consigue compilar y ejecutar únicamente el código correspondiente al modo indicado, que en este caso ejecutará un bucle de manera secuencial, el número de iteraciones indicado por la CLI. Esto es el fragmento de código siguiente:

```c++
#if MODE == 0
  for (auto i=0; i<numIter; i++) {
    resul += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
  }
  ellipse = resul * gap * 4.082482905;
#endif
```
A continuación, solo se debe imprimir por pantalla el resultado obtenido.

Tal y como veremos en el apartado 3, sólo con estos pocos cambios ya se consigue un código mucho más optimo respecto al inicial.

## 2.2. C++

Por otro lado, para el caso de la paralelización con c++, se debe compilar el código en MODE = 1. El código desarrollado para este modo, en primer lugar comprueba el número de threads que se han introducido desde la CLI (bien desde una variables de entorno o bien como parámetro). Si el parámetro es nThreads = 1, la ejecución se hará de manera secuencial, mientras que de no ser así, se crearán n threads, y se calculará el número de ejecuciones que debe hacer cada uno de ellos. Los threads ejecutarán la función calculaElipse, que tiene la misma función que el bucle del secuencial. Cada thread sumará su resultado parcial al global, que se encuentra en una variable global. Esta se encuentra protegida por un mutex, de forma que sólo pueda acceder a ella un thread de la vez. Además, en caso de que el número de elementos para cada thread no sea un número entero, el thread 0 se encargará de ejecutar la función para los elementos sobrantes. Esto lo vemos en el siguiente fragmento de código:

```c++
#if MODE == 1

  ... // Comprobación del número de threads

  if (numIter < (long)nThreads){
    calculaElipse(0,numIter,gap);
  }

  ...

  std::thread threads[nThreads];
  // Ejecuta los threads
  for(auto i=0; i<nThreads; i++){
      threads[i] = std::thread(calculaElipse, ini, fin, gap);
      ini += numEleT;
      fin += numEleT;
  }
  if (resto != 0){
    threads[0] = std::thread(calculaElipse, ini, fin+resto, gap);
  }

  ...

  ellipse = resulG * gap * 4.082482905;

#endif
```

A continuación, solo se debe imprimir por pantalla el resultado obtenido.

Con la creación de threads conseguimos que cada uno de ellos ejecute una cantidad definida de cálculos, ejecutando por tanto el código de manera paralela en los diferentes threads, consiguiedo optimizar el código, tal y como se ve en el análisis de los resultados [Apartado 3].

## 2.3. OpenMP

Por último, se pide la paralelización del código con OpenMP. Para su ejecución, se deberá compilar el código con MODE = 3. De la misma forma que en el caso anterior, se debe leer el número de threads que se ha pasado desde la CLI (o bien como parámetro o bien como variable de entorno). A continuación, comienza una región paralela con cláusula parallel, en la que se definen como variables compartidas numIter y gap, ya que son valores ya definidos, e iguales para todas las ejecuciones. Además, se define como privada la variable nT, que identifica al thread, siendo por tanto único y privado para cada uno de ellos. Esto lo vemos en el siguiente fragmento de código:

```c++
#if MODE == 2

  ...  // Comprobación del número de threads

  int nT;
  omp_set_num_threads(nThreads); // Asigna el número de threads a utilizar
  #pragma omp parallel shared(numIter,gap) private(nT) reduction(+:resul)
  {
      nT = omp_get_thread_num();
      for (int i=nT; i<numIter; i+=nThreads) {
          resul += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
      }
  };
  ellipse = resul * gap * 4.082482905;
#endif
```
Finalmente, solo se debe imprimir por pantalla el resultado obtenido.

Utilizando esta cláusula parallel conseguimos que el código que se encuentra restringido en ella se divida en pequeñas tareas a ejecutar por tantos threads como se han indicado. De esta forma, se consigue realizar el calculo de manera paralela, consiguiendo un código mucho más óptimo.


## 3. Metodología y desarrollo de las pruebas realizadas

En primer lugar, como prueba básica de funcionamiento se ha probado que, tanto el programa secuencial proporcionado, como los tres modos desarrollados durante esta práctica calculaban siempre el mismo resultado.

Después, el objetivo principal de las pruebas ha sido comprobar las diferentes optimizaciones del código. Para ello, se ha utilizado la función gettimeofday al principio y final del código, puediendo obtener así el tiempo de ejecución en cada caso.

En todas las pruebas comentadas a continuación, se han ejecutado con los benchmarks del anexo, para N = 1000000 y N = 100000000, con ejecuciones en dos estaciones, y con una pequeña espera entre ejecuciones. En cada caso se ha ejecutado 11 veces por estación, descartando siempre la primera de ellas ya que se entiende como calentamiento de la máquina.

En primer lugar, se han comparado el código proporcionado con el desarrollado en la práctica en modo secuencial. Los resultados del speedup obtenido se representan en la siguiente gráfica. En ella vemos como hay una mejora muy notable para el caso del N de menor tamaño. Para el N mayor, aunque el speedup sigue siendo considerable, el notable aumento de la carga computacional de las ejecución se ve reflejado en los resultados.

<img src="graficaSecuenciales.png" width="500" style="display:block; margin:0 auto;"/>

El gráfico siguiente compara el código secuencial ya optimizado con el paralelo desarrollado con c++. Los dos bloques de la izquierda representan las estaciones de las ejecuciones para el N pequeño, mientras que los dos de la derecha muestran las estaciones para el N mayor. Tal como indica la leyenda, se ha ejecutado para 1, 2, 4 y 8 threads. Vemos que para el primer caso, el speedup es muy estable para cualquier número de threads, mientras que para el N grande, la mayor optimización se consigue con 4 threads, y la peor con 1 thread. Estos son unos resultados lógicos ya que con 1 único thread se ejecuta el código de manera secuencial, mientras que con 4 threads se aprovechan los cores de la máquina en la que se han realizado las pruebas.

<img src="graficaSecuencialC.png" width="500" style="display:block; margin:0 auto;"/>

El gráfico siguiente se puede explicar de manera parecida, ya que realiza una comparación del código secuencial optimizado con el paralelo con OpenMP. Los speedups para el N menor son algo peores que en el caso anterior, pero para el N mayor los resultados son muy similares, teniendo resultados algo mejores para 8 threads.

<img src="graficaSecuencialOMP.png" width="500" style="display:block; margin:0 auto;"/>

Por último, se adjunta una gráfica con los resultados de los speedups obtenidos al comparar el secuencial optimizado con el secuencial de c++ y con el OpenMP. Esta gráfica se pone para comprar rápidamente de forma visual los resultados obtenidos, pero no aporta ningún dato relevante que no se haya comentado previamente.

<img src="graficaSpeedups.png" width="500" style="display:block; margin:0 auto;"/>

## 4. Discusión

- Explica qué optimizaciones has realizado en cada modelo de programación con respecto al código original secuencial.

En el caso secuencial, simplemente se ha optado por no utilizar structs con los datos, y todos los números de las variables que no varían a lo largo de la ejecución, y que no dependen de los parámetros introducidos, se han operado directamente. También se ha decidido no utilizar el fichero hpp, ya que con el código implementado sólo se podría declarar en él la función calculaElipse. Todo esto se encuentra también explicado en el apartado 2.

- Explica cómo has hecho los benchmarks y qué metodología de medición has usado.

El benchmark utilizado ejecuta el código de manera secuencial, paralela con 1, 2, 4 y 8 threads, y con paralelismo mediante openMP también con 1, 2, 4 y 8 threads. Esto lo hace en dos estaciones, con una breve espera entre cada ejecución. Además, se realiza para dos números de iteraciones diferentes (100000 y 100000000).

Además, se ha ejecutado también el código a optimizar en las mismas condiciones y con los mismos parámetros. De esta forma, se han podido realizar correctamente las comparaciones de las pruebas realizadas vistas en el apartado anterior.

El benchmark desarrollado se puede consultar tanto en la carpeta de la práctica como en el anexo.

## Anexos

**NOTA 1**: A continuación se encuentran los benchmarks utilizados y a los que se hace referencia en la tercera sección de este informe. Se han añadido a este anexo por simplicidad y claridad del informe.

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
size1=1000000
size2=100000000
case "$1" in
  seq1)
      build/seq $size1
      ;;
  seq2)
      build/seq $size2
      ;;

  mul11)
      build/cpp $size1 1
      ;;
  mul12)
      build/cpp $size1 2
      ;;
  mul14)
      build/cpp $size1 4
      ;;
  mul18)
      build/cpp $size1 8
      ;;
    ...

  omp11)
      build/omp $size1 1
      ;;
  omp12)
      build/omp  $size1 2
      ;;
  omp14)
      build/omp  $size1 4
      ;;
  omp18)
      build/omp  $size1 8
      ;;
    ...

esac
```

**launcher.sh:**

```sh
file=resultsInc.csv
touch $file
	for size in seq1 seq2; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

	for size in mul11 mul12 mul14 mul18 mul21 mul22 mul24 mul28; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

	for size in omp11 omp12 omp14 omp18 omp21 omp22 omp24 omp28; do
	  printf "$size," >> $file
	  for i in `seq 1 1 11`; do
	    ./benchsuite.sh $size >> $file
	  done
	  printf "\n" >> $file
	  sleep 1
	done

```
