#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>

/**
* Práctica 2: Optimizing
* Elena Romón López
**/

/*
MODE == 0 -> secuencial
MODE == 1 -> paralelo nThreads
MODE == 2 -> OPEN_MP
*/

#if MODE == 1
#include <mutex>
#include <thread>
std::mutex g_m;
double resulG = 0.0;
void calculaElipse(int ini, int fin, double gap);
#endif

#if MODE == 2
#include <omp.h>
#endif

/*
*  Programa principal
*/
int main(int argc, char* argv[]){

  // Comprobación de parámetros
  if(argc < 2 || argc > 3){
      printf("ERROR: Parámetros mal introducidos.\n");
      exit(0);
  }

  int numIter = atol(argv[1]);

  // Comprobación del número de iteraciones introducido
  if (numIter <= 0){
    printf("ERROR: El número de iteraciones no es válido.\n");
    exit(0);
  }

  struct timeval tm;
  gettimeofday( &tm, NULL );
  double TiempoIni =(double)tm.tv_sec + (double)tm.tv_usec / 1000000.0;

  double gap = 1.0/(double)numIter;
  double resul = 0.0;
  double ellipse;


// Caso secuencial
#if MODE == 0
  for (auto i=0; i<numIter; i++) {
    resul += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
  }
  ellipse = resul * gap * 4.082482905;
  printf("Ellipse = %f\n", ellipse);
#endif

// Caso paralelo (std::threads)
#if MODE == 1

  int nThreads;
  // Comprobación del número de threads
  if(getenv("THREADS")) {
    nThreads = atoi(getenv("THREADS"));
  } else if(argc == 3){
    nThreads = atoi(argv[2]);
  }

  // Ejecuta en secuencial si no hay al menos una iteración por thread
  if (numIter < (long)nThreads){
    calculaElipse(0,numIter,gap);
  }

  double numEleT = numIter/nThreads;
  int resto = numIter - (numEleT * nThreads);

  int ini = 0;
  int fin = numEleT;
  std::thread threads[nThreads];
  // Ejecuta los threads
  for(auto i=0; i<nThreads; i++){
      threads[i] = std::thread(calculaElipse, ini, fin, gap);
      ini += numEleT;
      fin += numEleT;
  }
  // En caso de que las iteraciones no sean equitativas, el thread 0 ejecutará el resto
  if (resto != 0){
    threads[0] = std::thread(calculaElipse, ini, fin+resto, gap);
  }

  for (int i = 0; i < nThreads; ++i) {
        threads[i].join();
  }
  ellipse = resulG * gap * 4.082482905;
  printf("Ellipse = %f\n", ellipse);
#endif

// Paralelización con OPEN_MP
#if MODE == 2

  int nThreads;
  // Comprobación del número de threads
  if(getenv("THREADS")) {
    nThreads = atoi(getenv("THREADS"));
  } else if(argc == 3){
    nThreads = atoi(argv[2]);
  }

  int nT;
  omp_set_num_threads(nThreads); // Asigna el número de threads a utilizar
  #pragma omp parallel shared(numIter,gap) private(nT) reduction(+:resul)
  {
      nT = omp_get_thread_num();
      for (int i=nT; i<numIter; i+=nThreads) {
          resul += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
      }
  };
  ellipse = resul * gap * 4.082482905;
  printf("Ellipse = %f\n", ellipse);
#endif

  gettimeofday( &tm, NULL );
  double tiempoFin =(double)tm.tv_sec + (double)tm.tv_usec / 1000000.0;
  double tiempoEjec = tiempoFin - TiempoIni;
  printf("Tiempo de ejecución:%f\n",tiempoEjec);
}

//---------------------------------------------

#if MODE == 1
/**
* Función auxiliar que realiza los cálculos para el caso paralelo con threads
*/
void calculaElipse(int ini, int fin, double gap){
  double aux = 0;
  for (auto i=ini; i<fin; i++) {
    aux += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
  }
  std::lock_guard<std::mutex> guard(g_m);
  resulG += aux;
}
#endif
