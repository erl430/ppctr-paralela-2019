#!/usr/bin/env bash
case $1 in
	m1)
		./video_task_seq 1
		;;
	m2)
		./video_task_seq 5
		;;
	m3)
		./video_task_seq 10
		;;
	p1)
		./build/video_task 1
		;;
	p2)
		./build/video_task 5
		;;
	p3)
		./build/video_task 10
		;;
esac
