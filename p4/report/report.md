# PRÁCTICA 4: Filtrado de un vídeo

Autor: Elena Romón López

Fecha: 18/12/2019

## Prefacio

En esta práctica se ha implementado una optimización del código proporcionado mediante tareas de OpenMP del código proporcionado , implementado así un código de ejecución paralela.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Anexo

## 1. Sistema

El benchmark se ha ejecutado en el mismo sistema que las prácticas anteriores. En los informes de las mismas se podrán consultar las características del mismo.

## 2. Diseño e Implementación del Software

Para optimizar el código que se proporciona, se ha optado por paralelizar el bucle do while del main, ya que realiza múltiples llamadas a la función fgaus, que a sus vez realiza múltiples operaciones. Por tanto, se ha considerado este como el bloque que conlleva más tiempo de cómputo a la hora de la ejecución.

Se ha creado un bloque parallel que incluye a todo el bucle, de manera que todo lo que engloba el bloque se va a dividir en subtareas a ejecutar por los threads. la llamada a la función fread se realiza bajo un bloque master, de manera que sólo se ejecutará por un thread en cada iteración del bucle. Por otro lado, el contenido del if está en un bloque task, indicando que esa es una de las tareas a ejecutar por los threads. Tras ese bloque se encuentra una cláusula taskwait que indica que todos los threads deben esperar a que todos los threads hayan acabado de ejecutar sus tareas antes de continuar su ejecución.

De esta forma, se ha conseguido que todas las llamadas a la función fgauss se realicen de forma paralela, optimizando de forma considerable el código.

En el apartado 4 se responderá a las preguntas realizadas en el enunciado de la práctica

## 3. Metodología y desarrollo de las pruebas realizadas

Para realizar las pruebas se han ejecutado los benchmarks del anexo. Estos ejecutan tanto el programa secuencial como el paralelo, para buffers de tamañi 1, 5 y 10. Esto se hace en dos estaciones de 11 iteraciones cada una, de las que siempre se descarta la primera ya que se considera de calentamiento.

Con los resultado obtenidos se ha realizado la siguiente gráfica:

<img src="graficaP4.png" style="display:block; margin:0 auto;"/>

En ella observamos que para los valores establecidos, los speedups son más o menos constantes, aunque podemos ver que a mayor tamaño de buffer, menor speedup respecto al secuencial.

## 4. Discusión

A continuación se encuentran las respuestas a las preguntas planteadas en el enunciado de la práctica:

- Explica qué funcionalidad has tenido que añadir/modificar sobre el código original.

Con las modificaciones se consigue la paralelización del código. Es decir, este se va a dividir en tareas a ejecutar por threads, optimizando de manera notable los tiempos de cómputo de la ejecución total del código.

- Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado, salvo las que ya hayas explicado en la práctica anterior.

Como ya se ha comentado en el desarrollo 2, la directiva master hace que el bloque al que afecta se ejecute solamente por uno de los threads. Por otro lado, la directiva task hace que el código que se encuentra bajo ella se divida en tareas de manera que cada thread las ejecute, diviendo la carga de trabajo entre todos los threads existentes. Por último, la directiva taskwait hace que los threads esperen a que todos ellos acaben la ejecución de sus tareas.

- Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.

Se ha asignado la etiqueta shared a las variables heigth, width, in y out, ya que estas van a ser comunes para todos los threads. Sin embargo, la i será una variable private ya que para cada thread ejecutará las tareas para unos pixels diferentes.


- Explica cómo se consigue el paralelismo en este programa (en la función main).

El paralelismo se consigue mediante las directivas de OpenMP mencionadas en la pregunta anterior, que divide el trabajo en tareas que son ejecutadas por los diferentes threads.

- Calcula la ganancia (speedup) obtenido con la paralelización y explica los resultados obtenidos.






FALTAAAAAAAAAAAAAAAAA









- ¿Se te ocurre alguna optimización sobre la función fgauss? Explica qué has hecho y qué ganancia supone con respecto a la versión paralela anterior.

Durante la realización de la práctica se han hecho diferentes pruebas para tratar de optimizar esta función (por ejemplo, mediante un bloque omp for), pero los resultados obtenidos no son los más optimos. Para conseguir un código optimizado, es más eficiente paralelizar las llamadas a esta función tal y como se encuentra en el código entregado.

## Anexos

A continuación se encuentran los benchmarks que se han ejecutado para comparar los resultados en el apartado 3.

**launcher.sh:**

```sh
#!/usr/bin/env bash
file=resultsInc.csv
touch $file
	for size in m1 m2 m3 p1 p2 p3; do
		printf "$size," >> $file
		for i in `seq 1 1 11`; do
			./benchsuite.sh $size >> $file
		done
		printf "\n" >> $file
		sleep 1
	done

```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
case $1 in
	m1)
		./video_task_seq 1
		;;
	m2)
		./video_task_seq 5
		;;
	m3)
		./video_task_seq 10
		;;
	p1)
		./build/video_task 1
		;;
	p2)
		./build/video_task 5
		;;
	p3)
		./build/video_task 10
		;;
esac
```
