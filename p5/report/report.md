# Práctica 5: Fractal de Mandelbrot

Autor: Elena Romón López

Fecha: 19/12/2019

## Prefacio

En esta práctica se pide realizar profiling del código proporcionado, y en función de los resultados obtenidos, evaluar las necesidades de cómputo de los diferentes bloques de código, y tratar de optimizarlos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Anexo

## 1. Sistema

Los benchmarks se han realizado en la misma máquina que en las prácticas anteriores. Las características se pueden consultar en el informe de las prácticas 1 y 2.

Nota: en la práctica 1 no se encuentran las flags de la CPU.

## 2. Diseño e Implementación del Software

### Ejercicio 1

En primer lugar, se ha realizado un análisis del código proporcionado. Dicho análisis se ha hecho utilizando valgrind, tal como se pide. Los resultados del análisis son los siguientes:

<img src="valgrind.png" style="display:block; margin:0 auto;"/>

Vemos aquí que hay un gran cuello de botella en la función explode. Sin embargo, analizando también el código de dicha función, vemos que el cómputo en su interior no es excesivo. Por tanto, se ha realizado un análisis más profundo del código, en el que se ha visto que existen muchas llamadas a esta función, y que es por esto que genera el cuello de botella, y no tanto por tener un cómputo muy pesado en su interior. Por tanto, se ha optado por paralelizar el bloque en el que se hacen las llamadas a esta función.

Por otro lado, las líneas que no se ejecutan nunca son las que se encuentran dentro de un if de comprobación de los parámetros introducidos, o de comprobación del estado de los ficheros generados. Esto lo hemos averiguado tras realizar el test de cobertura.

### Ejercicio 2

Para paralelizar el código dado, se ha empezado a trabajar con los for de la línea 71 (del código ya optimizado). Estos se han recogido en un bloque parallel, en el que se han declarado como compartidas las variables que no cambian para la ejecución de las diferentes tareas, y como private las que recorren el bucle, o se les asigna un nuevo valor en cada iteración. Dentro de la región paralela, se ha declarado una región pragma for schedule, que por un lado indica que el bucle ha de ejecutarse en paralelo, mientras que la cláusula schedule(dynamic) hace que las iteraciones se vayan asignando a los threads cuando acaban su trabajo, de manera dinámica. A continuación se encuentra un fragmento del código paralelizado:

```c++
#pragma omp parallel shared(n, x_max, x_min, y_max, y_min, count, count_max) private(i,j,x,y)
{
  #pragma omp for schedule(dynamic)
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      ...
    }
  }
```
### Ejercicio 3

Por último, se pide realizar cuatro implementaciones diferentes con el bloque de código siguiente al paralelizado en la actividad anterior. Aquí se encuentra una breve descripción del código realizado, mientras que los speedups conseguidos se verán en el apartado 3 de esta memoria.

- Implementación mediante directivas OpenMP

Este primer caso se ha implementado de forma muy parecida al bucle for paralelizado en el ejercicio anterior. Se ha utilizado una sección for schedule(dynamic) para dividir el bucle en tareas y que vayan ejecutando los threads. Además, dentro del bucle for más interno se ha definido una sección crítica del código, que hace que la variable c_max quede protegida, y solo pueda ser accedida por un único thread cada vez.

- Implementación mediante funciones de runtime

En este caso, el bucle se ha paralelizado de la misma manera. Sin embargo, en esta ocasión la variable c_max se ha protegido mediante un mutex (funciones del runtime). El mutex se coge justo antes de la zona crítica, y se libera al finalizarla. Una vez que el fragmento se ha ejecutado por completo, se destruye el mutex.

- Implementación secuencial

Recordemos que en este tercer ejercicio, el fragmento de código que estamos modificando se encuentra dentro de la sección paralela. Por tanto, para conseguir una ejecución secuencial del fragmento, este se tiene que incluir en una sección single, haciendo así que la ejecute un único thread.

- Implementación con variables privadas de cada thread y selección del máximo de todas ellas

Por último, para ejecutar el fragmento mediante variables privadas para cada thread se ha paralelizado el bucle for de la misma forma que en los dos primeros casos. Sin embargo, la variable a calcular (c_max) va a ser sustituida por una variable local, en la que se calculará el resultado para cada tarea. A continuación, se comparará con la variable c_max en una sección crítica y si la local es mayor, se asigna su valor a c_max.

## 3. Metodología y desarrollo de las pruebas realizadas

En este caso se han realizado pruebas para todos los modos (y para la version secuencial proporcionada). Las pruebas se han realizado en dos estaciones, con 11 ejecuciones por estación, descartando siempre la primera (calentamiento de la máquina). Además, entre ejecuciones se ha realizado una pequeña pausa.

En la gráfica siguiente se ve la comparativa de speedups del secuencial sin optimizar frente a los optimizados en modo 1, 2, 3 y 4 en cada una de las estaciones. Vemos que 3 de las versiones obtienen una speedup parecido, siendo la secuencial (modo 3) la que tiene un speedup peor, es decir la que ejecuta de forma menos dispar al secuencial. Esto tiene sentido ya que se está ejecutando también de forma secuencial. El resto de casos tienen un speedup muy parecido, cosa normal teniendo en cuenta paralelizan el fragmento de forma muy similar. Todos ellos, de diferentes formas, recorren el bucle de forma paralela, y tienen cuidado en el acceso a la zona crítica, logrando de diferentes maneras que el acceso a la variable c_max se haga de manera individual.

<img src="grafica.png" style="display:block; margin:0 auto;"/>


## Anexos

A continuación se encuentran los benchmarks utilizados :

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
case $1 in
	m1)
		./video_task_seq 1
		;;
	m2)
		./video_task_seq 5
		;;
	m3)
		./video_task_seq 10
		;;
	p1)
		./build/video_task 1
		;;
	p2)
		./build/video_task 5
		;;
	p3)
		./build/video_task 10
		;;
esac
```

**launcher.sh:**

```sh
#!/usr/bin/env bash
file=resultsInc.csv
touch $file
	for size in m1 m2 m3 p1 p2 p3; do
		printf "$size," >> $file
		for i in `seq 1 1 11`; do
			./benchsuite.sh $size >> $file
		done
		printf "\n" >> $file
		sleep 1
	done

```
